@extends('Layouts/app')
@section('title')
  Register Page
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('Register')}}" method="post">
                    @csrf
                    <h1>Registration</h1>
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Firstname</label>
                            <input type="text" name="firstname" class="form-control" >
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Lastname</label>
                            <input type="text" name="lastname" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" name="email"  class="form-control" aria-describedby="emailHelp">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputEmail1">Phone</label>
                            <input type="text" name="phone" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Birth date</label>
                        <input type="date" name="birth_year"  class="form-control">
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" >
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="exampleInputPassword1">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control" >
                        </div>
                    </div>
                    <button type="submit" name="btn_submit" class="btn btn-primary mb-4">Register</button>
                </form>

            </div>
        </div>
    </div>
@endsection

