@extends('Layouts/app')
@section('content')
    <div class="container-fluid homeImage">
        <div class="row">
            <div class="col-3">
                <img src="{{asset('/storage/image/'.'images.png')}}" alt="img" class="vector">
                <h2 class="logo">Practic</h2>
            </div>
            <div class="col-9">
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Clients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection



